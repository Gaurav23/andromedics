package com.example.andromedics;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	EditText editemail;
	EditText editpwd;
	Button btnlogin;
	Button btnsignup;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		editemail=(EditText)this.findViewById(R.id.editemail);
		editpwd=(EditText)this.findViewById(R.id.editpwd);
		btnlogin=(Button)this.findViewById(R.id.btnlogin);
		btnsignup=(Button)this.findViewById(R.id.btnsingup);
		btnlogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(editemail.getText().toString().equals(editpwd.getText().toString())){
					Intent i=new Intent(MainActivity.this, Medsearch.class);
					startActivity(i);
				}else{
					Toast.makeText(MainActivity.this,"Invalid Email/Password",Toast.LENGTH_LONG).show();
				}
				
			}
		});
		btnsignup.setOnClickListener(new OnClickListener() { 
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(MainActivity.this, Signup.class);
				startActivity(i);

				finish();
						
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	

}