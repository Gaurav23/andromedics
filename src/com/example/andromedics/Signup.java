package com.example.andromedics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Signup extends Activity implements OnClickListener{
	EditText txtfname;
	EditText txtmname;
	EditText txtlname;
	EditText txtmono;
	EditText txtaddress;
	EditText txtusername;
	EditText txttypepwd;
	EditText txtretypepwd;
	EditText txtemail;
	EditText txtbdate;
	Button btnsubmit;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		txtfname=(EditText)this.findViewById(R.id.txtfname);
		txtmname=(EditText)this.findViewById(R.id.txtmname);
		txtlname=(EditText)this.findViewById(R.id.txtlname);
		txtmono=(EditText)this.findViewById(R.id.txtmono);
		txtaddress=(EditText)this.findViewById(R.id.txtaddress);
		txtusername=(EditText)this.findViewById(R.id.txtusername);
		txttypepwd=(EditText)this.findViewById(R.id.txttypepwd);
		txtretypepwd=(EditText)this.findViewById(R.id.txtretypepwd);
		txtemail=(EditText)this.findViewById(R.id.txtemail);
		txtbdate=(EditText)this.findViewById(R.id.txtbdate);
		btnsubmit=(Button)this.findViewById(R.id.btnsubmit);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(Signup.this, MainActivity.class);
				startActivity(i);
			}
		});
		
		
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	

	
	
	

}