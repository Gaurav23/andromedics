package com.example.andromedics;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;


import android.content.Context;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class Shopmap extends MapActivity{
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_map);
		 MapView view=(MapView) findViewById(R.id.shopmap);
		 view.setBuiltInZoomControls(true);
		 //view.setSatellite(true);
		 
		 final MapController control=view.getController();
		 
		 LocationManager manager=(LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
		 LocationListener ll=new LocationListener() {
			
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				control.setCenter(new GeoPoint((int)location.getLatitude(),(int) location.getLongitude()));
				
			}
		};
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,0,ll);
		
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	

}