package com.example.andromedics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class Medsearch extends Activity implements OnItemSelectedListener,TextWatcher,OnClickListener{
	private static String[] medicine=new String[]{"Abarelix","Abatacept","Abelcet","Abilify"};
	private static final String[] type={"Syrup","Bluetooth","Chrome","Docs","Email",
        "Facebook","Google","Hungary","Iphone","Korea","Machintosh",
        "Nokia","Orkut","Picasa","Singapore","Turkey","Windows","Youtube"};
	Button btnsearch;
	
	GPSTracker gps;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>
		(this,android.R.layout.simple_dropdown_item_1line,medicine);
		AutoCompleteTextView AutoMed = (AutoCompleteTextView)findViewById(R.id.txtsearch);
		AutoMed.addTextChangedListener(this);
		//AutoMed.setThreshold(3);
		AutoMed.setAdapter(adapter);
		Spinner category=(Spinner)findViewById(R.id.menucategory);
        category.setOnItemSelectedListener(this);
        ArrayAdapter<String> aa=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,type);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_item);
        category.setAdapter(aa);
        
        //Button search code
        btnsearch=(Button)this.findViewById(R.id.btnsearch);
        btnsearch.setOnClickListener(new OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 gps = new GPSTracker(Medsearch.this);
				 double latitude=0;
				 double longitude=0;
	                // check if GPS enabled     
	                if(gps.canGetLocation()){
	                     
	                    latitude = gps.getLatitude();
	                    longitude = gps.getLongitude();
	                    
	                     Toast.makeText(getApplicationContext(),
	                      "Your Location is - \nLat: " + latitude + "\nLong: " + longitude,
	                       Toast.LENGTH_LONG).show();
	                    // \n is for new line
	                      
	                }else{
	                    // can't get location
	                    // GPS or Network is not enabled
	                    // Ask user to enable GPS/network in settings
	                    gps.showSettingsAlert();
				
			}
	                Intent i=new Intent(Medsearch.this, ShopDetail.class);
	                i.putExtra("latitude",latitude+"");
	                i.putExtra("longitude",longitude+"");
    				startActivity(i);
	                
			}
		});
        
        
		
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	

}