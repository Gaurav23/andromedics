package com.example.andromedics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ShopDetail extends Activity implements OnClickListener{
	Button btnmap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_detail);
		TextView tvlagi=(TextView)findViewById(R.id.txtlatitude);
		tvlagi.setText( getIntent().getExtras().getString("latitude"));
		TextView tvlong=(TextView)findViewById(R.id.txtlongitude);
		tvlong.setText (getIntent().getExtras().getString("longitude"));
		btnmap=(Button)this.findViewById(R.id.btnmap);
		btnmap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(ShopDetail.this, Shopmap.class);
				startActivity(i);
			}
		});
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}